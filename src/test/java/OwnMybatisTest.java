import com.xc.pojo.User;
import com.xc.dao.IUserDao;
import org.junit.Test;
import xc.io.Resources;
import xc.sqlSessioin.SqlSession;
import xc.sqlSessioin.SqlSessionFactory;
import xc.sqlSessioin.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

/**
 * @Description
 * @Author xuchang
 * @Date 2022/10/9 22:56:04
 * @Version 1.0
 */
public class OwnMybatisTest {

    /**
     * @Description 传统方式，不用mapper代理
     * @Author xuchang
     * @Date 2022/10/9 22:58:19
     * 1、只需要一个实体类
     * 2、创建sqlSession，调用CRUD方法，需要参数statementId：namespace.id
     */
    @Test
    public void test1() throws Exception {
        // 1.根据配置文件的路径，加载成字节输入流，存到内存中 注意：配置文件还未解析
        InputStream inputStream = Resources.getResourceAsStream("sqlMapConfig.xml");

        // 2.解析配置文件，封装到Configuration对象，创建sqlSessionFactory工厂对象
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 3.生产sqlSession 创建执行器对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 4.调用sqlSession方法
        User user = new User();
        user.setId(100);
        user.setName("zhangsan");
        User userOne = sqlSession.selectOne("user.selectOne", user);
        System.out.println("查询单个用户："+userOne);

        List<User> userList = sqlSession.selectList("user.selectList",null);
        System.out.println("查询所有用户："+userList.toString());

        // 5.释放资源
        sqlSession.close();
    }

    /**
     * @Description mapper代理测试
     * @Author xuchang
     * @Date 2022/10/20 16:40:23
     * 1、实体类，接口（方法名 == namespace.id）==> 生成基于接口的代理类
     * 2、直接调用接口对应方法即可，不用再手动传入"namespace.id"
     */
    @Test
    public void test2() throws Exception {

        // 1.根据配置文件的路径，加载成字节输入流，存到内存中 注意：配置文件还未解析
        InputStream resourceAsSteam = Resources.getResourceAsStream("sqlMapConfig.xml");

        // 2.解析了配置文件，封装了Configuration对象  2.创建sqlSessionFactory工厂对象
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);

        // 3.生产sqlSession 创建了执行器对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 4.调用sqlSession方法
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);


      /*  User user1 = new User();
        user1.setId(1);
        user1.setUsername("tom");
        User user3 = userDao.findByCondition(user1);
        System.out.println(user3);*/
        List<User> all = userDao.findAll();
        for (User user : all) {
            System.out.println(user);
        }

        // 5.释放资源
        sqlSession.close();


    }
}
