package com.xc.pojo;

import lombok.Data;

/**
 * @Description 用户表
 * @Author xuchang
 * @Date 2022/10/9 22:36:44
 * @Version 1.0
 */
@Data
public class User {
    private Integer id;
    private String name;
}
